<?php

/**
 * @file
 * Add a preprocessor to prepare data from FlexSlider Views
 */
function template_preprocess_views_view_mapbox(&$vars) {
  drupal_add_js('//api.tiles.mapbox.com/mapbox.js/v1.3.1/mapbox.js', array('type' => 'external'));
  drupal_add_css('//api.tiles.mapbox.com/mapbox.js/v1.3.1/mapbox.css', array('type' => 'external'));
}

/**
 * Add a preprocessor to prepare data from FlexSlider Views
 */
function template_process_views_view_mapbox(&$vars) {

  $this_module = drupal_get_path('module', 'node_mapbox');
  drupal_add_js('//api.tiles.mapbox.com/mapbox.js/v1.3.1/mapbox.js', array('type' => 'external'));
  drupal_add_css('//api.tiles.mapbox.com/mapbox.js/v1.3.1/mapbox.css', array('type' => 'external'));
  drupal_add_js($this_module . '/node_mapbox.js');
  $result = $vars['view']->result;
  $lat_field = $vars['options']['mapbox']['latitude'];
  $lon_field = $vars['options']['mapbox']['longitude'];
  $center_lat = $vars['options']['mapbox']['map_center_lat'];
  $center_lon = $vars['options']['mapbox']['map_center_lon'];
  $map_zoom = $vars['options']['mapbox']['map_zoom'];
  // $popup_field = $vars['options']['mapbox']['popup'];
  $colour_field = $vars['options']['mapbox']['color'];

  foreach ($result as $key => $value) {
    $lat = 0;
    if (!empty($value->_field_data['nid']['entity']->{$lat_field})) {
      $lat = strip_tags($value->_field_data['nid']['entity']->{$lat_field}['und'][0]['value']);
    }

    $lon = 0;
    if (!empty($value->_field_data['nid']['entity']->{$lon_field})) {
      $lon = strip_tags($value->_field_data['nid']['entity']->{$lon_field}['und'][0]['value']);
    }
    $colour = '333333';
    if (!empty($value->_field_data['nid']['entity']->{$colour_field})) {
      $colour = strip_tags($value->_field_data['nid']['entity']->{$colour_field}['und'][0]['value']);
    }
    $popup = $vars['rows'][$key];

    $markers[$lat . '|' . $lon][] = array(
      'type' => 'Feature',
      'content' => '<div class="popup">' . $popup . '</div>',
      'lat' => $lat,
      'lon' => $lon,
      'colour' => $colour,
    );
  }
  foreach ($markers as $key => $pin) {
    $content = '';
    foreach ($pin as $item) {
      $content .= $item['content'];
    }
    list($lat, $lon) = explode('|', $key);

    $pins [] = array(
      'type' => 'Feature',
      'content' => '<div class="has-' . count($pin) . '-content clearfix">' . $content . '</div>',
      'lat' => $lat,
      'lon' => $lon,
      'colour' => $item['colour'],
    );
  }

  $settings = array(
    'markers' => $pins,
    'mapbox_id' => $vars['options']['mapbox']['mapbox_id']
  );
  if ($center_lat != '' && $center_lon != '') {
    $settings['centers'] = array($center_lat, $center_lon);
  }
  if (!empty($map_zoom)) {
    $settings['zoom'] = $map_zoom;
  }
  drupal_add_js(array('mapbox' => $settings), 'setting');

  return $vars;
}
