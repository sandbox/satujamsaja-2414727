<?php

/**
 * @file
 * Contains the Mapbox style plugin.
 */
class views_plugin_style_mapbox_views extends views_plugin_style {

  /**
   * Set default options.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options += array('mapbox' => array(
        'mapbox_id' => array('default' => ''),
        'popup' => array('default' => ''),
        'latitude' => array('default' => ''),
        'longitude' => array('default' => ''),
    ));
    return $options;
  }

  function options_form(&$form, &$form_state) {

    parent::options_form($form, $form_state);

    $form['mapbox'] = array(
      '#type' => 'fieldset',
      '#title' => t('Mapbox'),
    );

    $fields = array('' => t('None'));
    foreach ($this->view->display_handler->get_handlers('field') as $field => $handler) {
      $fields[$field] = $handler->ui_name();
    }

    $form['mapbox'] = array(
      'mapbox_id' => array(
        '#title' => t('Mapbox Map ID'),
        '#type' => 'textfield',
        '#description' => t('Enter your Mapbox ID'),
        '#default_value' => $this->options['mapbox']['mapbox_id'],
      ),
      'info_window' => array(
        '#type' => 'select',
        '#title' => t('Info Window'),
        '#description' => t('Select window popup content'),
        '#options' => $fields,
        '#default_value' => $this->options['mapbox']['info_window'],
      ),
      'color' => array(
        '#type' => 'select',
        '#title' => t('Icon Color'),
        '#description' => t('Select Icon Color'),
        '#options' => $fields,
        '#default_value' => $this->options['mapbox']['color'],
      ),
      'latitude' => array(
        '#type' => 'select',
        '#title' => t('Latitude Field'),
        '#options' => $fields,
        '#description' => t('Module will use this field as latitude Value'),
        '#default_value' => $this->options['mapbox']['latitude'],
      ),
      'longitude' => array(
        '#type' => 'select',
        '#options' => $fields,
        '#title' => t('Longitude Field'),
        '#description' => t('Module will use this field as longitude Value'),
        '#default_value' => $this->options['mapbox']['longitude'],
      ),
      'map_center_lat' => array(
        '#type' => 'textfield',
        '#title' => t('Map Center Latitude'),
        '#description' => t('Module will use this value as map center latitude Value'),
        '#default_value' => $this->options['mapbox']['map_center_lat'],
      ),
      'map_center_lon' => array(
        '#type' => 'textfield',
        '#title' => t('Map Center Longitude'),
        '#description' => t('Module will use this value as map center longitude Value'),
        '#default_value' => $this->options['mapbox']['map_center_lon'],
      ),
      'map_zoom' => array(
        '#type' => 'textfield',
        '#title' => t('Map Zoom'),
        '#description' => t('Module will use this value as map zoom Value'),
        '#default_value' => $this->options['mapbox']['map_zoom'],
      ),
    );
  }

}
