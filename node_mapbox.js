/**
 * @file
 * Provides the map integration
 */

var map = null;
(function($) {
    Drupal.behaviors.node_mapbox = {
        attach: function(context, settings) {

            $('#map').once('map-init', function() {
                if ($('#views-preview-wrapper').length <= 0) {

                    if (typeof L == 'undefined') {
                        //Mapbox not loaded
                        return;
                    }

                    var geo_json = Drupal.settings.mapbox.markers;
                    $('#map').css('min-height', 666);
                    map = L.mapbox.map('map', Drupal.settings.mapbox.mapbox_id);
                    map.scrollWheelZoom.disable();
                    var selected_marker_index = null;
                    var markers = new Array();
                    $(geo_json).each(function(i, item) {
                        if (item.lat && item.lon) {
                            var ic = L.icon({
                                iconUrl: 'https://api.tiles.mapbox.com/v3/marker/pin-m+' + item.colour + '.png',
                                iconSize: [30, 70]
                            });
                            marker = L.marker([item.lat, item.lon], {
                                icon: ic,
                                index: i
                            });
                            var opt = {
                                offset: new L.Point(0, -53),
                                closeButton: false
                            };
                            marker.bindPopup(item.content, opt);

                            markers.push(marker);
                        }

                    });

                    var features = L.featureGroup(markers);
                    features.addTo(map);
                    if (typeof Drupal.settings.mapbox.centers != 'undefined') {
                        if (typeof Drupal.settings.mapbox.zoom == 'undefined') {
                            Drupal.settings.mapbox.zoom = 2;
                        }
                        var latlng = L.latLng(Drupal.settings.mapbox.centers[0], Drupal.settings.mapbox.centers[1]);
                        map.setView(latlng, Drupal.settings.mapbox.zoom);
                    } else {
                        map.fitBounds(features.getBounds());
                    }
                }
            });

        }

    };

})(jQuery);