<?php

/**
 * @file
 * Provide views data and handlers for node_mapbox.module.
 */

/**
 * Implements hook_views_plugins().
 */
function node_mapbox_views_plugins() {
  return array(
    'style' => array(
      // Style plugin for the FlexSlider.
      'node_mapbox' => array(
        'title' => t('Nodes Mapbox'),
        'help' => t('Display content in a Mapbox.'),
        'handler' => 'views_plugin_style_mapbox_views',
        'theme' => 'views_view_mapbox',
        'theme file' => 'node_mapbox.theme.inc',
        'theme path' => drupal_get_path('module', 'node_mapbox') . '/theme',
        'uses row plugin' => TRUE,
        'uses fields' => TRUE,
        'uses pager' => FALSE,
        'uses ajax' => FALSE,
        'uses more' => FALSE,
        'uses options' => TRUE,
        'type' => 'normal',
        'even empty' => FALSE,
      ),
    ),
  );
}
